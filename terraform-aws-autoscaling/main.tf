# Description : This Script is used to create EC2, EIP.

#Module      : Label
#Description : This terraform module is designed to generate consistent label names and
#              tags for resources. You can use terraform-labels to implement a strict
#              naming convention.
module "labels" {
  source  = "../terraform-aws-labels"

  name        = var.name
  repository  = var.repository
  environment = var.environment
  managedby   = var.managedby
  label_order = var.label_order
}

# Module      : Launch configuration
# Description : Terraform module to create an Launch configuration resource on AWS
resource "aws_launch_configuration" "default" {
  count = var.create_lc ? 1 : 0

  name_prefix                 = "${coalesce(var.lc_name, var.name)}-"
  image_id                    = var.image_id
  instance_type               = var.instance_type

  key_name                    = var.key_name
  security_groups             = var.security_groups
  associate_public_ip_address = var.associate_public_ip_address
  user_data                   = var.user_data
  enable_monitoring           = var.enable_monitoring
  spot_price                  = var.spot_price
  placement_tenancy           = var.spot_price == "" ? var.placement_tenancy : ""
  ebs_optimized               = var.ebs_optimized

  lifecycle {
    create_before_destroy = true
  }
}

# Module      : Autoscaling group
# Description : Terraform module to create an Autoscaling group resource on AWS
resource "aws_autoscaling_group" "default" {
  count = var.create_asg ? 1 : 0

  name_prefix          = coalesce(var.asg_name, var.name)

  launch_configuration = var.create_lc ? element(aws_launch_configuration.default.*.name, 0) : var.launch_configuration
  vpc_zone_identifier  = var.vpc_zone_identifier
  max_size             = var.max_size
  min_size             = var.min_size
  desired_capacity     = var.desired_capacity

  load_balancers            = var.load_balancers
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type

  target_group_arns         = var.target_group_arns
  default_cooldown          = var.default_cooldown
  force_delete              = var.force_delete
  termination_policies      = var.termination_policies
  suspended_processes       = var.suspended_processes
  placement_group           = var.placement_group

  wait_for_capacity_timeout = var.wait_for_capacity_timeout
  protect_from_scale_in     = var.protect_from_scale_in


  dynamic "tag" {
    for_each = module.labels.tags
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }


  lifecycle {
    create_before_destroy = true
    ignore_changes        = [desired_capacity]
  }
}


# # Get instance IPs for Ansible
# data "aws_instances" "instance_ips" {

#   instance_state_names = ["pending", "running"]
#   filter {
#     name   = "tag:Name"
#     values = ["asg-webdenav"]
#   }
# }
