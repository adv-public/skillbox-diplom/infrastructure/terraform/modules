## terraform-aws-subnet

## Prerequisites

This module has a few dependencies: 

- [Terraform 1.x.x](https://learn.hashicorp.com/terraform/getting-started/install.html)
- [Go](https://golang.org/doc/install)
- [github.com/stretchr/testify/assert](https://github.com/stretchr/testify)
- [github.com/gruntwork-io/terratest/modules/terraform](https://github.com/gruntwork-io/terratest)

## Examples


**IMPORTANT:** Since the `master` branch used in `source` varies based on new modifications, we suggest that you use the release versions [here](https://github.com/clouddrove/terraform-aws-subnet/releases).


Here are some examples of how you can use this module in your inventory structure:
### Private Subnet
```hcl
  module "subnets" {
    source              = "clouddrove/terraform-aws-subnet/aws"
    version             = "0.15.3"
    name                = "subnets"
    environment         = "test"
    label_order         = ["name", "environment"]
    availability_zones  = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
    vpc_id              = "vpc-xxxxxxxxx"
    type                = "private"
    nat_gateway_enabled = true
    cidr_block          = "10.0.0.0/16"
    ipv6_cidr_block     = module.vpc.ipv6_cidr_block
    public_subnet_ids   = ["subnet-XXXXXXXXXXXXX", "subnet-XXXXXXXXXXXXX"]
  }
```

### Public-Private Subnet
```hcl
  module "subnets" {
    source              = "clouddrove/terraform-aws-subnet/aws"
    version             = "0.15.3"
    name                = "subnets"
    environment         = "test"
    label_order         = ["name", "environment"]
    availability_zones  = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
    vpc_id              = "vpc-xxxxxxxxx"
    type                = "public-private"
    igw_id              = "ig-xxxxxxxxx"
    nat_gateway_enabled = true
    cidr_block          = "10.0.0.0/16"
    ipv6_cidr_block     = module.vpc.ipv6_cidr_block
  }
```

  ### Public-Private Subnet with single Nat Gateway
```hcl
  module "subnets" {
    source              = "clouddrove/terraform-aws-subnet/aws"
    version             = "0.15.3"
    name                = "subnets"
    environment         = "test"
    label_order         = ["name", "environment"]
    availability_zones  = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
    vpc_id              = "vpc-xxxxxxxxx"
    type                = "public-private"
    igw_id              = "ig-xxxxxxxxx"
    nat_gateway_enabled = true
    single_nat_gateway  = true
    cidr_block          = "10.0.0.0/16"
    ipv6_cidr_block     = module.vpc.ipv6_cidr_block
  }
```

### Public Subnet
```hcl
  module "subnets" {
    source              = "clouddrove/terraform-aws-subnet/aws"
    version             = "0.15.3"
    name                = "subnets"
    environment         = "test"
    label_order         = ["name", "environment"]
    availability_zones  = ["us-east-1a", "us-east-1b", "us-east-1c"]
    vpc_id              = "vpc-xxxxxxxxx"
    type                = "public"
    igw_id              = "ig-xxxxxxxxx"
    cidr_block          = "10.0.0.0/16"
    ipv6_cidr_block     = module.vpc.ipv6_cidr_block
  }
```
### Public-private-subnet-single-nat-gateway
```hcl
  module "subnets" {
    source              = "clouddrove/terraform-aws-subnet/aws"
    version             = "0.15.3"
    nat_gateway_enabled = true
    single_nat_gateway  = true
    name                = "subnets"
    environment         = "example"
    label_order         = ["name", "environment"]
    availability_zones  = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
    vpc_id              = "vpc-xxxxxxxxxx"
    type                = "public-private"
    igw_id              = "ig-xxxxxxxxxxx"
    cidr_block          = "10.0.0.0/16"
    ipv6_cidr_block     = module.vpc.ipv6_cidr_block
  }
```






## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| assign\_ipv6\_address\_on\_creation | Specify true to indicate that network interfaces created in the specified subnet should be assigned an IPv6 address. | `bool` | `false` | no |
| attributes | Additional attributes (e.g. `1`). | `list(any)` | `[]` | no |
| availability\_zones | List of Availability Zones (e.g. `['us-east-1a', 'us-east-1b', 'us-east-1c']`). | `list(string)` | `[]` | no |
| az\_ngw\_count | Count of items in the `az_ngw_ids` map. Needs to be explicitly provided since Terraform currently can't use dynamic count on computed resources from different modules. https://github.com/hashicorp/terraform/issues/10857. | `number` | `0` | no |
| az\_ngw\_ids | Only for private subnets. Map of AZ names to NAT Gateway IDs that are used as default routes when creating private subnets. | `map(string)` | `{}` | no |
| cidr\_block | Base CIDR block which is divided into subnet CIDR blocks (e.g. `10.0.0.0/16`). | `string` | n/a | yes |
| delimiter | Delimiter to be used between `organization`, `environment`, `name` and `attributes`. | `string` | `"-"` | no |
| enable\_acl | Set to false to prevent the module from creating any resources. | `bool` | `true` | no |
| enable\_flow\_log | Enable subnet\_flow\_log logs. | `bool` | `false` | no |
| enabled | Set to false to prevent the module from creating any resources. | `bool` | `true` | no |
| environment | Environment (e.g. `prod`, `dev`, `staging`). | `string` | `""` | no |
| igw\_id | Internet Gateway ID that is used as a default route when creating public subnets (e.g. `igw-9c26a123`). | `string` | `""` | no |
| ipv4\_private\_cidrs | Subnet CIDR blocks (e.g. `10.0.0.0/16`). | `list(any)` | `[]` | no |
| ipv4\_public\_cidrs | Subnet CIDR blocks (e.g. `10.0.0.0/16`). | `list(any)` | `[]` | no |
| ipv6\_cidr\_block | Base CIDR block which is divided into subnet CIDR blocks (e.g. `10.0.0.0/16`). | `string` | n/a | yes |
| ipv6\_cidrs | Subnet CIDR blocks (e.g. `2a05:d018:832:ca02::/64`). | `list(any)` | `[]` | no |
| label\_order | Label order, e.g. `name`,`application`. | `list(any)` | `[]` | no |
| managedby | ManagedBy, eg 'CloudDrove'. | `string` | `"hello@clouddrove.com"` | no |
| map\_public\_ip\_on\_launch | Specify true to indicate that instances launched into the subnet should be assigned a public IP address. | `bool` | `true` | no |
| max\_subnets | Maximum number of subnets that can be created. The variable is used for CIDR blocks calculation. | `number` | `6` | no |
| name | Name  (e.g. `app` or `cluster`). | `string` | `""` | no |
| nat\_gateway\_enabled | Flag to enable/disable NAT Gateways creation in public subnets. | `bool` | `false` | no |
| private\_network\_acl\_id | Network ACL ID that is added to the private subnets. If empty, a new ACL will be created. | `string` | `""` | no |
| public\_network\_acl\_id | Network ACL ID that is added to the public subnets. If empty, a new ACL will be created. | `string` | `""` | no |
| public\_subnet\_ids | A list of public subnet ids. | `list(string)` | `[]` | no |
| repository | Terraform current module repo | `string` | `"https://github.com/clouddrove/terraform-aws-subnet"` | no |
| s3\_bucket\_arn | S3 ARN for vpc logs. | `string` | `""` | no |
| single\_nat\_gateway | n/a | `bool` | `false` | no |
| tags | Additional tags (e.g. map(`BusinessUnit`,`XYZ`). | `map(any)` | `{}` | no |
| traffic\_type | Type of traffic to capture. Valid values: ACCEPT,REJECT, ALL. | `string` | `"ALL"` | no |
| type | Type of subnets to create (`private` or `public`). | `string` | `""` | no |
| vpc\_id | VPC ID. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| private\_acl | n/a |
| private\_route\_tables\_id | The ID of the routing table. |
| private\_subnet\_cidrs | CIDR blocks of the created private subnets. |
| private\_subnet\_cidrs\_ipv6 | CIDR blocks of the created private subnets. |
| private\_subnet\_id | The ID of the private subnet. |
| private\_tags | A mapping of private tags to assign to the resource. |
| public\_acl | n/a |
| public\_route\_tables\_id | The ID of the routing table. |
| public\_subnet\_cidrs | CIDR blocks of the created public subnets. |
| public\_subnet\_cidrs\_ipv6 | CIDR blocks of the created public subnets. |
| public\_subnet\_id | The ID of the subnet. |
| public\_tags | A mapping of public tags to assign to the resource. |

### Resource
[clouddrove](https://github.com/clouddrove/terraform-aws-subnet)